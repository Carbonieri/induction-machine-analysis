# Induction Machine Analysis

Working directories containing all the tools for a rapid induction machine analysis, linking MATLAB to the FEA software FEMM.

In the folder RFOAnalysis, the code for the induction motor simulation using static FEA.

In the folder EqCircAnalysis, the code for the classical analysis using the equivalent circuit.