%% INDUCTION MOTOR ON-LOAD STATIC SIMULATION

% This script allows the IM on-load permormance prediction, performing
% magneto-static analyses. The rotor is computed using the iterative
% procedure, based on th machine model in the Rotor Field Oriented (RFO)
% reference frame. This procedure is done in the file RFOA.m.
%
% Here you need:
%  1. MOTOR_DATA_single_cage.m loaded:
%  2. launch the function for the stator winding and rotor cage parameters
%     computation;
%  3. MOST IMPORTANT! Define stator d- and q-axis currents
%
%
% 2020/04/08
% *************************************************************************

%%

clc; clear; close all;

MOTOR_DATA_single_cage;

% specify the parameter SB = 1 if u are using the sliding band air-gap
% boundary condition, otherwise set it to 0:
SB = 1;

%% machine specifications

K_fe = stator.K_fe; % packing factor
Tstator = 120; % stator temperature
Trotor  = 120; % rotor temperature

Vnom = 400; % [V] specifify the pahse voltage
fnom = 50; % [Hz]b stator frequency

%% winding parameters

[Lew, Rs, sigma_cage, sigma_cage_20, Kring, Sslot_S, Sslot_R, Lring, Rring] =...
  calc_windings_parameters1(Tstator, Trotor, filename, stator, rotor);
stator.winding.R = Rs;
stator.winding.Lew = Lew;
rotor.winding.Kring = Kring;

%% LOCKED-ROTOR SIMULATIONS

% The aim of this analysis is to derive the variation of the rotor
% resistance and the overall leakage inductance with the slip frequency, to
% take into consideration the effect of the current crowding inside the
% rotor bars.

if ~exist('IM_LR_Res.mat', 'file')
  
  freq_vect = [0 0.25 0.5 0.75 1 1.25 1.50 1.75 2.0 2.5 4 5 10 30 50];
  
  [R12_vect, L12_vect, omega_vect, Leq_vect, Req_vect] =...
    IM_LR(freq_vect, sigma_cage, stator, rotor, SB);
  load('IM_LR_Res.mat', 'R12_vect', 'L12_vect', 'omega_vect', 'Leq_vect', 'Req_vect', 'Torque_vect', 'Tmxw_vect');
else
  load('IM_LR_Res.mat', 'R12_vect', 'L12_vect', 'omega_vect','Leq_vect', 'Req_vect', 'Torque_vect', 'Tmxw_vect');
end

% figures
figure(), grid on; hold on; box on;
p1 = plot(omega_vect/(2*pi), R12_vect, 'linewidth', 1.5);
p2 = plot(omega_vect/(2*pi), Req_vect, '--', 'linewidth', 1.5);
legend([p1; p2], 'Rotor Resistance (\Gamma circuit)', 'Equivalent Resistance', 'location', 'best')
title('Locked Rotor Analysis: Rotor Resistance')
xlabel('Rotor Frequency [Hz]')
ylabel('Resistance [\Omega]')

figure(), grid on; hold on; box on;
p1 = plot(omega_vect/(2*pi), L12_vect, 'linewidth', 1.5);
p2 = plot(omega_vect/(2*pi), Leq_vect, '--x', 'linewidth', 1);
legend([p1; p2], 'Leakage inductance (\Gamma circuit)', 'Equivalent Inductance', 'location', 'best')
title('Locked Rotor Analysis: Leakahe Inductance')
xlabel('Rotor Frequency [Hz]')
ylabel('Inductance [H]')

figure(), grid on; hold on;
p1 = plot(omega_vect/(2*pi), Tmxw_vect, 'linewidth', 1.5);
p2 = plot(omega_vect/(2*pi), Torque_vect, '--x');
legend([p1; p2], 'Torque via Maxwell stress tensor', 'T = p*P_{Jr}/(2*\pi*f_{sim})', 'location', 'best')

%% NO-LOAD SIMULATIONS

% The aim of this analysis is to derive the stator inductance as function
% of the magnetizing current, considering the saturation effect.

if exist('temp','dir')
  delete('temp\temp.ans')
end

% filename:
filename = 'IM_single_cage_SB';

% the no-load simulations are magneto-static:
freq_sim = 0;

% Choose the minimum effective value of the magnetizing current:
Imu_min = 0.1;

% Choose the maximum effective value of the magnetizing current:
Imu_max = 6;

% Choose the number of simulation:
Nsim = 20;

% Calculate the current delta for consecutive simulations:
deltaI = (Imu_max-Imu_min)/(Nsim-1);

% Define the current vector for the simulatons:
Imu_vect = [ Imu_min:deltaI:Imu_max ];

if exist('IM_NL_res.txt','file') == 0
  % initialize vectors:
  fluxS_vect = zeros(size(Imu_vect));
  Energy_vect = zeros(size(Imu_vect));
  AJint_vect = zeros(size(Imu_vect));
  Vs_vect = zeros(size(Imu_vect));
  LsLambda_vect = zeros(size(Imu_vect));
  LsEn_vect = zeros(size(Imu_vect));
  LsAJ_vect = zeros(size(Imu_vect));
  
  openfemm(1);
  for ImuIdx = 1:max(size(Imu_vect))
    
    % define the peak value of the phase current:
    Imu_peak = Imu_vect(ImuIdx)*sqrt(2); % [A]
    psi = 0;
    
    % define the phase current phasors, considering a certain initial angle
    % psi. In the static simulations, only the real part is considered.
    Isa_Re = Imu_peak*cos(psi);
    Isb_Re = Imu_peak*cos(psi-(2/3)*pi);
    Isc_Re = Imu_peak*cos(psi-(4/3)*pi);
    
    Isa_Im = Imu_peak*sin(psi);
    Isb_Im = Imu_peak*sin(psi-(2/3)*pi);
    Isc_Im = Imu_peak*sin(psi-(4/3)*pi);
    
    isa = Isa_Re + 1i*Isa_Im;
    isb = Isb_Re + 1i*Isb_Im;
    isc = Isc_Re + 1i*Isc_Im;
    
    opendocument([filename,'.fem']); % open the model
    solving_core;
    
    % compute the amplitude of the stator flux linkage space vector:
    fluxS = (2/3)*(FluxABC_S(1)-0.5*FluxABC_S(2)-0.5*FluxABC_S(3));
    
    % compute the voltage:
    Vs = fluxS*2*pi*fnom/sqrt(2);
    
    % compute the stator synchronous inductance using the flux linkage:
    LsLambda = fluxS/Imu_peak;
    
    % compute the stator inductance using the stored energy:
    LsEn = (4/3)*Energy/Imu_peak^2;
    
    % compute the stator inductance using the integral A*J:
    LsAJ = (2/3)*AJint/Imu_peak^2;
    
    fluxS_vect(ImuIdx) = fluxS;
    Energy_vect(ImuIdx) = Energy;
    AJint_vect(ImuIdx) = AJint;
    Vs_vect(ImuIdx) = Vs;
    LsLambda_vect(ImuIdx) = LsLambda;
    LsEn_vect(ImuIdx) = LsEn;
    LsAJ_vect(ImuIdx) = LsAJ;
    
    % WRITING THE RESULTS IN RESULT FILE
    
    % Colomn      |      Quantity
    %-------------|---------------
    %   1         |        Imu [A] rms
    %   2         |       fluxS [V s]
    %   3         |       Energy [J]
    %   4         |       AJint [J]
    %   5         |        Eav [V]
    %   6         |      LsLambda [H]
    %   7         |        LsEn [H]
    %   8         |        LsAJ [H]
    
    if ~exist('FileSuffix','var')
      FileSuffix = '';
    end
    OutputData = [ Imu_peak/sqrt(2), fluxS, Energy, AJint,...
                   Vs, LsLambda, LsEn, LsAJ ];
    OutputFile = 'IM_NL_res';
    fid = fopen([OutputFile,'',FileSuffix,'.txt'],'at');
    %
    Nout = length(OutputData);
    OutputFormat = [repmat(['%+1.9f','   '],1,2), ...
      repmat(['%+1.9e','   '],1, Nout - 2),...
      '\n'];
    
    fprintf(fid, OutputFormat, OutputData);
    fclose(fid);
  end
  closefemm()
end

% figures

NLmtrx = load('IM_NL_res.txt');

figure(), grid on; hold on; box on;
plot(NLmtrx(:,1), NLmtrx(:,5), 'linewidth', 1.5)
xlabel('magnetizing current [A] rms')
ylabel('phase voltage [V] rms')

figure(), grid on; hold on; box on;
p1 = plot(NLmtrx(:,1), NLmtrx(:,3), 'linewidth', 1.5);
p2 = plot(NLmtrx(:,1), NLmtrx(:,4), '--', 'linewidth', 1.5);
legend([p1; p2], 'Magnetic Energy', 'Integral A*J/2', 'location', 'best')
xlabel('magnetizing current [A] rms')
ylabel('Energy [J]')

figure(), grid on; hold on; box on;
p1 = plot(NLmtrx(:,1), NLmtrx(:,6), 'linewidth', 1.5);
p2 = plot(NLmtrx(:,1), NLmtrx(:,7), '-x', 'linewidth', 1.5);
p3 = plot(NLmtrx(:,1), NLmtrx(:,8), '--', 'linewidth', 1.5);
legend([p1; p2; p3], 'L_{s} from flux linkage', 'L_{s} from energy', 'L_{s} from A*J', 'location', 'best')
xlabel('magnetizing current [A] rms')
ylabel('stator inductance [H]')

%% analysis using the equivalent circuit (Gamma circuit)

% define the vector of the stator phase voltage vs magnetizing current:
Eavv_vect = NLmtrx(:,5);

% define the slip vector
slip_min = min(omega_vect)/(2*pi*fnom);
slip_max = 1;
NumSim = 500;
delta_s = (slip_max-slip_min)/NumSim;
slip_vect = slip_min:delta_s:slip_max;

% initialize arrays:
Z12_vect = zeros(1,max(size(slip_vect)));
Is_vect = zeros(1,max(size(slip_vect)));
Imag_vect = zeros(1,max(size(slip_vect)));
I12_vect = zeros(1,max(size(slip_vect)));
PJr_vect = zeros(1,max(size(slip_vect)));
PJs_vect = zeros(1,max(size(slip_vect)));
T_vect = zeros(1,max(size(slip_vect)));

% define the stator impedance:
Z1 = Rs + 1i*2*pi*fnom*Lew;

% compute characteristics as function of the slip
for islipIdx = 1:max(size(slip_vect))
    fr = fnom*slip_vect(islipIdx);
    Z12_vect(islipIdx) = interp1(omega_vect/(2*pi),R12_vect,fr)/slip_vect(islipIdx) +...
       1i*interp1(omega_vect/(2*pi),L12_vect,fr)*2*pi*fnom;
   
    nn   = 0;
    nmax =  500;
    Eavv =  Vnom;
    Vavv =  Vnom;
    Vavv_cmplx  =  Vnom*(1.1);
    while (nn<nmax) && (abs(Vavv_cmplx)>Vavv)  
          Eavv = Eavv-0.1;
          Imu = -1i*interp1(Eavv_vect,Imu_vect,Eavv);
          I12 = Eavv/Z12_vect(islipIdx);
          I1 = Imu + I12;
          deltaV_cmplx = Z1*I1;
          Vavv_cmplx = Eavv+deltaV_cmplx;
          nn = nn+1; 
    end
    Imag_vect(islipIdx) = abs(Imu);
    I12_vect(islipIdx) = abs(I12);
    Is_vect(islipIdx) = abs(I1);
    PJs_vect(islipIdx) = 3*Rs*(abs(I1))^2;
    PJr_vect(islipIdx) = 3*real(Z12_vect(islipIdx))*(abs(I12))^2*slip_vect(islipIdx);
    T_vect(islipIdx) = 3*real(Z12_vect(islipIdx))*abs(I12)^2/(2*pi*fnom);
end

% figures

figure(), grid on; hold on; box on;
rpm0 = 60*fnom/stator.p;
rpm_vect = rpm0*(ones(1,max(slip_vect))-slip_vect);
p1 = plot(rpm_vect,T_vect,'','LineWidth',1.5);
p2 = plot(rpm_vect,Is_vect,'--','LineWidth',1.5);
title('Torque & Current Vs Speed Characteristic')
xlabel('run per minute')
ylabel('Torque [N m], Stator Current [A]')
legend([p1; p2], 'Torque','Stator Current', 'location', 'best')
hold off