function [R12_vect, L12_vect, omega_vect, Leq_vect, Req_vect] = IM_LR(freq_vect, sigma_cage, stator, rotor, SB)

if exist('IM_LR_RES.txt','file')
  delete IM_LR_RES.txt;
end

if exist('temp','dir')
  delete('temp\temp.ans')
end

% Enable Time Harmonic analysis:
LRanalysis = 1;

% femm file for time harmonic simulations
filename = 'IM_single_cage_LR_SB';

sim_poles = stator.sim_poles;
sim_period = stator.sim_period;

%% STATOR CURRENT PHASOR FOR LOCKED ROTOR SIMULATION
 
Is_LR = 10;
psi = 0;

Isa_Re = sqrt(2)*Is_LR*cos(psi);
Isb_Re = sqrt(2)*Is_LR*cos(psi-(2/3)*pi);
Isc_Re = sqrt(2)*Is_LR*cos(psi-(4/3)*pi);

Isa_Im = sqrt(2)*Is_LR*sin(psi);
Isb_Im = sqrt(2)*Is_LR*sin(psi-(2/3)*pi);
Isc_Im = sqrt(2)*Is_LR*sin(psi-(4/3)*pi);

isa = Isa_Re + 1i*Isa_Im;
isb = Isb_Re + 1i*Isb_Im;
isc = Isc_Re + 1i*Isc_Im;

%% SIMULATIONS VARYING THE FREQUENCY

openfemm(1);
for ff = 1:max(size(freq_vect))
  
  freq_sim = freq_vect(ff);
  opendocument([filename,'.fem']);
  % modify the cage conductivity according to the temperature:
  mi_modifymaterial(rotor.slot.material.name, 5, sigma_cage);
  solving_core;
  mi_close()
  
  % Colomn      |      Quantity
  %-------------|---------------
  %   1         |        frequency
  %   2         |        Is_LR
  %   3         |        Energy
  %   4         |        Tmxw
  %   5         |        Torque
  %   6         |        RotLosses
  %   7         |        Leq
  %   8         |        Req
  
  if exist('LRanalysis','var') && LRanalysis == 1
    if ~exist('FileSuffix','var')
      FileSuffix = '';
    end
    
    OutputData = [ freq_sim, Is_LR, Energy,Tmxw,Torque,RotLosses,Leq,Req];
    OutputFile = 'IM_LR_RES';
    fid = fopen([OutputFile,'',FileSuffix,'.txt'],'at');
    %
    Nout = length(OutputData);
    OutputFormat = [repmat(['%+3.2f','   '],1), ...
      repmat(['%+1.9e','   '],1, Nout - 1),...
      '\n'];
    
    fprintf(fid, OutputFormat, OutputData);
    fclose(fid);
  end
end
closefemm()

%% RESULTS ANALISYS

CC_mtrx = load('IM_LR_RES.txt');
freq_vect   = CC_mtrx(2:end,1);
Is_LR_vect  = CC_mtrx(2:end,2);
En_vect     = CC_mtrx(2:end,3);
Tmxw_vect   = CC_mtrx(2:end,4); % from Maxwell stress tensor
Torque_vect = CC_mtrx(2:end,5);
PJr_vect    = CC_mtrx(2:end,6);
Leq_vect    = CC_mtrx(2:end,7);
Req_vect    = CC_mtrx(2:end,8);

freq_vect   = (freq_vect(1:end));
omega_vect  = freq_vect*(2*pi);
Lmag_CC     = CC_mtrx(1,7);
Xmag_vect   = Lmag_CC*omega_vect;

Zeq_vect = Req_vect + 1i*Leq_vect.*omega_vect;
Z12_vect = Zeq_vect.*(1i*Xmag_vect)./(1i*Xmag_vect-Zeq_vect);
R12_vect = real(Z12_vect)*(1+rotor.winding.Kring);
L12_vect = imag(Z12_vect)./omega_vect;

% omega_vect = [0 omega_vect];
% R12_vect = [R12_vect(1) R12_vect]

save('IM_LR_Res.mat', 'R12_vect', 'L12_vect', 'omega_vect', 'Leq_vect', 'Req_vect', 'Torque_vect', 'Tmxw_vect');
end